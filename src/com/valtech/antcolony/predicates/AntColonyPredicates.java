package com.valtech.antcolony.predicates;

import java.util.function.Predicate;

import com.valtech.antcolony.constants.AntColonyConstants;
import com.valtech.antcolony.dto.Ant;

/**
 * This class contains all the predicates required in this application
 * @author simranjitsingh
 *
 */
public class AntColonyPredicates {
	
	public static Predicate<String> TOWARDS_HOME = status -> status.equalsIgnoreCase(AntColonyConstants.TOWARDS_HOME);
	
	public static Predicate<String> TOWARDS_LOCATION = status -> status.equalsIgnoreCase(AntColonyConstants.TOWARDS_LOCATION);
	
	public static Predicate<String> SEARCH_FOOD = status -> status.equalsIgnoreCase(AntColonyConstants.SEARCH_FOOD);
	
	public static Predicate<String> ANT_FOUND = status -> status.equalsIgnoreCase(AntColonyConstants.ANT_FOUND);
	
	public static Predicate<String> FOOD_FOUND = status -> status.equalsIgnoreCase(AntColonyConstants.FOOD_FOUND);
	
	public static Predicate<Ant> X_INCREASE = tempAnt -> tempAnt.getMovementX().equalsIgnoreCase(AntColonyConstants.INCREASE);
	
	public static Predicate<Ant> X_DECREASE = tempAnt -> tempAnt.getMovementX().equalsIgnoreCase(AntColonyConstants.DECREASE);
	
	public static Predicate<Ant> Y_INCREASE = tempAnt -> tempAnt.getMovementY().equalsIgnoreCase(AntColonyConstants.INCREASE);
	
	public static Predicate<Ant> Y_DECREASE = tempAnt -> tempAnt.getMovementY().equalsIgnoreCase(AntColonyConstants.DECREASE);
}
