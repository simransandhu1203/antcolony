package com.valtech.antcolony.dao.impl;

import com.valtech.antcolony.constants.AntColonyConstants;
import com.valtech.antcolony.dao.AntDao;
import com.valtech.antcolony.dto.Ant;
import com.valtech.antcolony.dto.Home;
import com.valtech.antcolony.dto.Position;

public class DefaultAntDaoImpl implements AntDao {
	
	/**
	 * this function loads an Ant from the given set of values
	 * @param currentPosInputX current position in X axis of an Ant
	 * @param currentPosInputY current position in Y axis of an Ant
	 * @param homePosInputX Home position in X axis of an Ant
	 * @param homePosInputY Home position in Y axis of an Ant
	 * @return
	 */
	public Ant addAnt(final Integer currentPosInputX, final Integer currentPosInputY, final Integer homePosInputX, final Integer homePosInputY) {
		Ant ant = new Ant();
		Position currentPos = new Position();
		currentPos.setPositionX(currentPosInputX);
		currentPos.setPositionY(currentPosInputY);
		ant.setCurrentPos(currentPos);
		Position homePos = new Position();
		homePos.setPositionX(homePosInputX);
		homePos.setPositionY(homePosInputY);
		Home home = new Home();
		home.setFoodQty(0);
		home.setPosition(homePos);
		ant.setHome(home);
		ant.setCarryingFood(false);
		ant.setMovementX(AntColonyConstants.INCREASE);
		ant.setMovementY(AntColonyConstants.INCREASE);
		return ant;
	}
	
}
