package com.valtech.antcolony.dao.impl;

import java.util.ArrayList;

import com.valtech.antcolony.dao.TribeDao;
import com.valtech.antcolony.dto.Ant;
import com.valtech.antcolony.dto.Position;
import com.valtech.antcolony.dto.Size;
import com.valtech.antcolony.dto.Tribe;

public class DefaultTribeDaoImpl implements TribeDao {
	
	/**
	 * this function loads a Tribe from the given set of inputs
	 * @param posInputX starting position in X axis of a Tribe
	 * @param posInputY starting position in Y axis of a Tribe
	 * @param sizeInputX length in X axis of a Tribe
	 * @param sizeInputY length in Y axis of a Tribe
	 * @return
	 */
	public Tribe addTribe(final Integer posInputX, final Integer posInputY, final Integer sizeInputX, final Integer sizeInputY) {
		Tribe tribe = new Tribe();
		Position position = new Position();
		position.setPositionX(posInputX);
		position.setPositionY(posInputY);
		tribe.setStartPos(position);
		Size size = new Size();
		size.setXSize(sizeInputX);
		size.setPositionY(sizeInputY);
		tribe.setSize(size);
		ArrayList<Ant> ants = new ArrayList<>();
		tribe.setAnts(ants);
		return tribe;
	}
}
