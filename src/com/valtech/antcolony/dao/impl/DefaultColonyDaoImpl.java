package com.valtech.antcolony.dao.impl;

import java.util.ArrayList;

import com.valtech.antcolony.dao.ColonyDao;
import com.valtech.antcolony.dto.Colony;
import com.valtech.antcolony.dto.Food;
import com.valtech.antcolony.dto.Size;
import com.valtech.antcolony.dto.Tribe;

public class DefaultColonyDaoImpl implements ColonyDao {

	/**
	 * this function loads a Colony from the given set of inputs
	 * @param sizeInputX length in X axis of a Colony
	 * @param sizeInputY length in Y axis of a Colony
	 * @return
	 */
	public Colony addColony(final Integer sizeInputX, final Integer sizeInputY) {
		Colony colony = new Colony();
		Size size = new Size();
		size.setXSize(sizeInputX);
		size.setPositionY(sizeInputY);
		colony.setSize(size);
		ArrayList<Tribe> tribes = new ArrayList<>();
		colony.setTribes(tribes);
		ArrayList<Food> foods = new ArrayList<>();
		colony.setFoods(foods);
		return colony;
	}
	
}
