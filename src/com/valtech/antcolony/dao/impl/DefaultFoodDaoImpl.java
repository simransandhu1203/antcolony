package com.valtech.antcolony.dao.impl;

import com.valtech.antcolony.dao.FoodDao;
import com.valtech.antcolony.dto.Food;
import com.valtech.antcolony.dto.Position;

public class DefaultFoodDaoImpl implements FoodDao {

	/**
	 * This function loads the Food from the given set of inputs
	 * @param posInputX current position in X axis of the Food
	 * @param posInputY current position in Y axis of the Food
	 * @param qty Quantity of food at that position
	 * @return
	 */
	public Food addFood(final Integer posInputX, final Integer posInputY, final Integer qty) {
		Food food = new Food();
		food.setFoodQty(qty);
		Position position = new Position();
		position.setPositionX(posInputX);
		position.setPositionY(posInputY);
		food.setPosition(position);
		return food;
	}
	
}
