package com.valtech.antcolony.dao;

import com.valtech.antcolony.dto.Ant;

public interface AntDao {
	/**
	 * this function loads an Ant from the given set of values
	 * @param currentPosInputX current position in X axis of an Ant
	 * @param currentPosInputY current position in Y axis of an Ant
	 * @param homePosInputX Home position in X axis of an Ant
	 * @param homePosInputY Home position in Y axis of an Ant
	 * @return
	 */
	public Ant addAnt(final Integer currentPosInputX, final Integer currentPosInputY, final Integer homePosInputX, final Integer homePosInputY);
}
