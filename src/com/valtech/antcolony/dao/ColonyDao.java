package com.valtech.antcolony.dao;

import com.valtech.antcolony.dto.Colony;

public interface ColonyDao {
	
	/**
	 * this function loads a Colony from the given set of inputs
	 * @param sizeInputX length in X axis of a Colony
	 * @param sizeInputY length in Y axis of a Colony
	 * @return
	 */
	public Colony addColony(final Integer sizeInputX, final Integer sizeInputY);
}
