package com.valtech.antcolony.dto;

public class Position {
	private Integer PositionX;
	private Integer PositionY;
	public Integer getPositionX() {
		return PositionX;
	}
	public void setPositionX(Integer positionX) {
		PositionX = positionX;
	}
	public Integer getPositionY() {
		return PositionY;
	}
	public void setPositionY(Integer positionY) {
		PositionY = positionY;
	}
}
