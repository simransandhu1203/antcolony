package com.valtech.antcolony.dto;

public class Ant {
	private Integer id;
	private Position currentPos;
	private boolean isCarryingFood;
	private Home home;
	private Position hasKnownLocation;
	private String movementX;
	public String getMovementX() {
		return movementX;
	}
	public void setMovementX(String movementX) {
		this.movementX = movementX;
	}
	public String getMovementY() {
		return movementY;
	}
	public void setMovementY(String movementY) {
		this.movementY = movementY;
	}
	private String movementY;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Position getCurrentPos() {
		return currentPos;
	}
	public void setCurrentPos(Position currentPos) {
		this.currentPos = currentPos;
	}
	public boolean isCarryingFood() {
		return isCarryingFood;
	}
	public void setCarryingFood(boolean isCarryingFood) {
		this.isCarryingFood = isCarryingFood;
	}
	public Home getHome() {
		return home;
	}
	public void setHome(Home home) {
		this.home = home;
	}
	public Position getHasKnownLocation() {
		return hasKnownLocation;
	}
	public void setHasKnownLocation(Position hasKnownLocation) {
		this.hasKnownLocation = hasKnownLocation;
	}
}
