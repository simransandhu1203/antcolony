package com.valtech.antcolony.dto;

public class Size {
	private Integer xSize;
	private Integer PositionY;
	public Integer getPositionX() {
		return xSize;
	}
	public void setXSize(Integer positionX) {
		xSize = positionX;
	}
	public Integer getPositionY() {
		return PositionY;
	}
	public void setPositionY(Integer positionY) {
		PositionY = positionY;
	}
}
