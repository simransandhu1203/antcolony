package com.valtech.antcolony.dto;

public class Home {
	private Integer foodQty;
	private Position position;
	public Integer getFoodQty() {
		return foodQty;
	}
	public void setFoodQty(Integer foodQty) {
		this.foodQty = foodQty;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
}
