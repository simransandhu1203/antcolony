package com.valtech.antcolony.dto;

import java.util.ArrayList;

public class Tribe {
	private Integer id;
	private ArrayList<Ant> ants;
	private Size size;
	private Position startPos;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ArrayList<Ant> getAnts() {
		return ants;
	}
	public void setAnts(ArrayList<Ant> ants) {
		this.ants = ants;
	}
	public Size getSize() {
		return size;
	}
	public void setSize(Size size) {
		this.size = size;
	}
	public Position getStartPos() {
		return startPos;
	}
	public void setStartPos(Position startPos) {
		this.startPos = startPos;
	}
}
