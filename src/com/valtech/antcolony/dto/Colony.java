package com.valtech.antcolony.dto;

import java.util.ArrayList;

public class Colony {
	private Size size;
	private ArrayList<Food> foods;
	private ArrayList<Tribe> tribes;
	public Size getSize() {
		return size;
	}
	public void setSize(Size size) {
		this.size = size;
	}
	public ArrayList<Food> getFoods() {
		return foods;
	}
	public void setFoods(ArrayList<Food> foods) {
		this.foods = foods;
	}
	public ArrayList<Tribe> getTribes() {
		return tribes;
	}
	public void setTribes(ArrayList<Tribe> tribes) {
		this.tribes = tribes;
	}
}
