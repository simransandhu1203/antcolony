package com.valtech.antcolony.dto;

public class Food {
	private Integer qty;
	private Position position;
	public Integer getFoodQty() {
		return qty;
	}
	public void setFoodQty(Integer foodQty) {
		this.qty = foodQty;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
}
