package com.valtech.antcolony.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

import com.valtech.antcolony.dto.Ant;
import com.valtech.antcolony.dto.Colony;
import com.valtech.antcolony.dto.Food;
import com.valtech.antcolony.dto.Tribe;
import com.valtech.antcolony.service.AntService;
import com.valtech.antcolony.service.ColonyService;
import com.valtech.antcolony.service.FoodService;
import com.valtech.antcolony.service.TribeService;
import com.valtech.antcolony.service.impl.DefaultAntServiceImpl;
import com.valtech.antcolony.service.impl.DefaultColonyServiceImpl;
import com.valtech.antcolony.service.impl.DefaultFoodServiceImpl;
import com.valtech.antcolony.service.impl.DefaultTribeServiceImpl;
import com.valtech.antcolony.util.AntColonyUtils;

public class AntColonyController {
	
	AntService antService = new DefaultAntServiceImpl();
	ColonyService colonyService = new DefaultColonyServiceImpl();;
	FoodService foodService = new DefaultFoodServiceImpl();
	TribeService tribeService = new DefaultTribeServiceImpl();
	Colony colony;
	
	/**
	 * the main function where execution starts
	 * It takes all the input from user and starts the processing of the game 
	 * @param args
	 */
	public static void main(String[] args) {
		AntColonyController antColonyController = new AntColonyController();
		antColonyController.readInput();
		antColonyController.searchFood();
	}
	
	public void readInput() {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Please enter the size of colony in X and Y format");
			String inputX = null;
			String inputY = null;
			while(!AntColonyUtils.isStringInteger(inputX)) {
				inputX = bufferedReader.readLine();
			}
			while(!AntColonyUtils.isStringInteger(inputY)) {
				inputY = bufferedReader.readLine();
			}
			colony = colonyService.addColony(Integer.parseInt(inputX), Integer.parseInt(inputY));
	
			String numberOfFoods = null;
			System.out.println("Please enter the number of foods available");
			while(!AntColonyUtils.isStringInteger(numberOfFoods)) {
				numberOfFoods = bufferedReader.readLine();
			}
			
			for(int i=0; i<Integer.parseInt(numberOfFoods); i++) {
				System.out.println("Please enter the location of food in X and Y format");
				inputX = null;
				inputY = null;
				while(!AntColonyUtils.isStringInteger(inputX)) {
					inputX = bufferedReader.readLine();
				}
				while(!AntColonyUtils.isStringInteger(inputY)) {
					inputY = bufferedReader.readLine();
				}
				
				System.out.println("Please enter the Quantity of Food in numeric format");
				String qtyInput = null;
				while(!AntColonyUtils.isStringInteger(qtyInput)) {
					qtyInput = bufferedReader.readLine();
				}
				final Food food = foodService.addFood(Integer.parseInt(inputX), Integer.parseInt(inputY), Integer.parseInt(qtyInput));
				colony.getFoods().add(food);
			}
			
			String numberOfTribes = null;
			System.out.println("Please enter the number of tribes available");
			while(!AntColonyUtils.isStringInteger(numberOfFoods)) {
				numberOfFoods = bufferedReader.readLine();
			}
			
			for(int i=0; i<Integer.parseInt(numberOfTribes); i++) {
				System.out.println("Please enter the location of tribe in X and Y format");
				String posInputX = null;
				String posInputY = null;
				while(!AntColonyUtils.isStringInteger(posInputX)) {
					posInputX = bufferedReader.readLine();
				}
				while(!AntColonyUtils.isStringInteger(posInputY)) {
					posInputY = bufferedReader.readLine();
				}
				
				System.out.println("Please enter the size of tribe in X and Y format");
				String sizeInputX = null;
				String sizeInputY = null;
				while(!AntColonyUtils.isStringInteger(sizeInputX)) {
					sizeInputX = bufferedReader.readLine();
				}
				while(!AntColonyUtils.isStringInteger(sizeInputY)) {
					sizeInputY = bufferedReader.readLine();
				}
				
				final Tribe tribe = tribeService.addTribe(Integer.parseInt(posInputX), Integer.parseInt(posInputY), Integer.parseInt(sizeInputX), Integer.parseInt(sizeInputY));
				
				String numberOfAnts = null;
				System.out.println("Please enter the number of Ants available for this tribe");
				while(!AntColonyUtils.isStringInteger(numberOfFoods)) {
					numberOfFoods = bufferedReader.readLine();
				}
				
				for(int j=0; j<Integer.parseInt(numberOfAnts); j++) {
					System.out.println("Please enter the current location of Ant in X and Y format");
					String currentPosInputX = null;
					String currentPosInputY = null;
					while(!AntColonyUtils.isStringInteger(currentPosInputX)) {
						currentPosInputX = bufferedReader.readLine();
					}
					while(!AntColonyUtils.isStringInteger(currentPosInputY)) {
						currentPosInputY = bufferedReader.readLine();
					}
					
					System.out.println("Please enter the home location of Ant in X and Y format");
					String homePosInputX = null;
					String homePosInputY = null;
					while(!AntColonyUtils.isStringInteger(homePosInputX)) {
						homePosInputX = bufferedReader.readLine();
					}
					while(!AntColonyUtils.isStringInteger(homePosInputY)) {
						homePosInputY = bufferedReader.readLine();
					}
					
					final Ant ant = antService.addAnt(Integer.parseInt(posInputX), Integer.parseInt(posInputY), Integer.parseInt(sizeInputX), Integer.parseInt(sizeInputY));
					tribe.getAnts().add(ant);
				}
				
				colony.getTribes().add(tribe);
				
			}
		} catch (IOException e) {
			System.out.println("There is some exception while getting input");
		}
	}
	
	public void searchFood() {
		while(colonyService.isFoodAvailable(colony)) {
			colony.getTribes()
			.stream()
			.filter(Objects::nonNull)
			.flatMap(tribe2 -> tribe2.getAnts().stream())
			.filter(Objects::nonNull)
			.forEach(ant -> {antService.move(ant, colony);});
		}
		
		System.out.println("All the foods in the colony are taken by the ants.");
	}
}
