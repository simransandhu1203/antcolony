package com.valtech.antcolony.util;

/**
 * this class contains the utility functions required in the application
 * @author simranjitsingh
 *
 */
public class AntColonyUtils {

	/**
	 * This function checks of the input given is an Integer or not
	 * @param input string to validate
	 * @return final result in boolean format
	 */
	public static boolean isStringInteger(String input) {
		if(input == null) {
			return false;
		}
		try {
			Integer.parseInt(input);
			return true;
		}
		catch(Exception e) {
			System.out.println("Please enter the input in Integer format");
			return false;
		}
	}
	
}
