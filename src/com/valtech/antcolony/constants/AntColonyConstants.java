package com.valtech.antcolony.constants;

/**
 * This class contains all the constants required in this application
 * @author simranjitsingh
 *
 */
public class AntColonyConstants {

	public static final String TOWARDS_HOME = "TOWARDS_HOME"; 
	
	public static final String FOOD_FOUND = "FOOD_FOUND"; 
	
	public static final String ANT_FOUND = "ANT_FOUND"; 
	
	public static final String SEARCH_FOOD = "SEARCH_FOOD"; 
	
	public static final String TOWARDS_LOCATION = "TOWARDS_LOCATION"; 
	
	public static final String INCREASE = "INCREASE"; 
	
	public static final String DECREASE = "DECREASE"; 
	
}
