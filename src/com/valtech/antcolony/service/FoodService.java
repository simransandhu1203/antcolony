package com.valtech.antcolony.service;

import com.valtech.antcolony.dto.Food;

public interface FoodService {
	
	/**
	 * This function loads the Food from the given set of inputs
	 * @param posInputX current position in X axis of the Food
	 * @param posInputY current position in Y axis of the Food
	 * @param qty Quantity of food at that position
	 * @return
	 */
	public Food addFood(final Integer posInputX, final Integer posInputY, final Integer qty);
}
