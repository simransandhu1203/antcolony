package com.valtech.antcolony.service.impl;

import com.valtech.antcolony.dao.ColonyDao;
import com.valtech.antcolony.dao.impl.DefaultColonyDaoImpl;
import com.valtech.antcolony.dto.Colony;
import com.valtech.antcolony.dto.Food;
import com.valtech.antcolony.service.ColonyService;

public class DefaultColonyServiceImpl implements ColonyService {
	
	ColonyDao colonyDao = new DefaultColonyDaoImpl();
	
	/**
	 * this function loads a Colony from the given set of inputs
	 * @param sizeInputX length in X axis of a Colony
	 * @param sizeInputY length in Y axis of a Colony
	 * @return
	 */
	public Colony addColony(final Integer sizeInputX, final Integer sizeInputY) {
		return this.colonyDao.addColony(sizeInputX, sizeInputY);
	}

	/**
	 * this function checks if the food is still available for ants in the colony
	 * @param colony the colony to be checked
	 * @return result in boolean format
	 */
	public boolean isFoodAvailable(final Colony colony) {
		for(Food food : colony.getFoods()) {
			if(food!= null && food.getFoodQty() > 0) {
				return true;
			}
		}
		return false;
	}
	
}
