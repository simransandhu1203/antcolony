package com.valtech.antcolony.service.impl;

import com.valtech.antcolony.dao.FoodDao;
import com.valtech.antcolony.dao.impl.DefaultFoodDaoImpl;
import com.valtech.antcolony.dto.Food;
import com.valtech.antcolony.service.FoodService;

public class DefaultFoodServiceImpl implements FoodService {
	
	FoodDao foodDao = new DefaultFoodDaoImpl();
	
	/**
	 * This function loads the Food from the given set of inputs
	 * @param posInputX current position in X axis of the Food
	 * @param posInputY current position in Y axis of the Food
	 * @param qty Quantity of food at that position
	 * @return
	 */
	public Food addFood(final Integer posInputX, final Integer posInputY, final Integer qty) {
		return this.foodDao.addFood(posInputX, posInputY, qty);
	}
}
