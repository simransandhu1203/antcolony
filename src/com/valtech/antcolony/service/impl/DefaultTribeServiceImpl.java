package com.valtech.antcolony.service.impl;

import com.valtech.antcolony.dao.TribeDao;
import com.valtech.antcolony.dao.impl.DefaultTribeDaoImpl;
import com.valtech.antcolony.dto.Tribe;
import com.valtech.antcolony.service.TribeService;

public class DefaultTribeServiceImpl implements TribeService {
	
	TribeDao tribeDao = new DefaultTribeDaoImpl();
	
	/**
	 * this function loads a Tribe from the given set of inputs
	 * @param posInputX starting position in X axis of a Tribe
	 * @param posInputY starting position in Y axis of a Tribe
	 * @param sizeInputX length in X axis of a Tribe
	 * @param sizeInputY length in Y axis of a Tribe
	 * @return
	 */
	public Tribe addTribe(final Integer posInputX, final Integer posInputY, final Integer sizeInputX, final Integer sizeInputY) {
		return tribeDao.addTribe(posInputX, posInputY, sizeInputX, sizeInputY);
	}

}
