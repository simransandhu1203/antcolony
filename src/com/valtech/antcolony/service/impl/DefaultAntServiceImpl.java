package com.valtech.antcolony.service.impl;

import java.util.Objects;
import com.valtech.antcolony.constants.AntColonyConstants;
import com.valtech.antcolony.dao.AntDao;
import com.valtech.antcolony.dao.impl.DefaultAntDaoImpl;
import com.valtech.antcolony.dto.Ant;
import com.valtech.antcolony.dto.Colony;
import com.valtech.antcolony.dto.Food;
import com.valtech.antcolony.predicates.AntColonyPredicates;
import com.valtech.antcolony.service.AntService;

public class DefaultAntServiceImpl implements AntService {
	
	AntDao antDao = new DefaultAntDaoImpl();
	
	/**
	 * this function initializes the movement of an ant
	 * @param ant ant to move
	 * @param colony whole colony available
	 */
	public void move(final Ant ant, final Colony colony) {
		if(ant.isCarryingFood()) {
			this.moveAnt(AntColonyConstants.TOWARDS_HOME, ant, colony, null, null);
			return;
		}
		
		Food food = this.isFoodVisible(ant, colony);
		if(food != null) {
			this.moveAnt(AntColonyConstants.FOOD_FOUND, ant, colony, food, null);
			return;
		}
		
		if(ant.getHasKnownLocation() == null) {
			Ant nearbyAnt = this.isAntVisible(ant, colony);
			if(nearbyAnt != null) {
				this.moveAnt(AntColonyConstants.ANT_FOUND, ant, colony, null, nearbyAnt);
				return;
			}
			this.moveAnt(AntColonyConstants.SEARCH_FOOD, ant, colony, null, null);
		}
		else {
			this.moveAnt(AntColonyConstants.TOWARDS_LOCATION, ant, colony, null, null);
		}
	}
	
	/**
	 * this function moves the ant depending on the current situation of ant
	 * @param attr current situation of ant
	 * @param ant ant to move
	 * @param colony colony available
	 * @param food food available nearby
	 * @param nearbyAnt and present nearby
	 */
	public void moveAnt(final String attr, final Ant ant, final Colony colony, final Food food, final Ant nearbyAnt) {
		
		if(AntColonyPredicates.TOWARDS_HOME.test(attr)) {
			if(ant.getHome().getPosition().getPositionX() == ant.getCurrentPos().getPositionX() && ant.getHome().getPosition().getPositionY() == ant.getCurrentPos().getPositionY()) {
				ant.getHome().setFoodQty(ant.getHome().getFoodQty() + 1);
				ant.setCarryingFood(false);
				this.moveAnt(AntColonyConstants.TOWARDS_LOCATION, ant, colony, food, nearbyAnt);
				return;
			}
			if(ant.getHome().getPosition().getPositionX() > ant.getCurrentPos().getPositionX()) {
				ant.getCurrentPos().setPositionX(ant.getCurrentPos().getPositionX() + 1);
			}
			else if(ant.getHome().getPosition().getPositionX() < ant.getCurrentPos().getPositionX()) {
				ant.getCurrentPos().setPositionX(ant.getCurrentPos().getPositionX() - 1);
			}
			if(ant.getHome().getPosition().getPositionY() > ant.getCurrentPos().getPositionY()) {
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() + 1);
			}
			else if(ant.getHome().getPosition().getPositionY() < ant.getCurrentPos().getPositionY()) {
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() - 1);
			}
		}
		else if(AntColonyPredicates.TOWARDS_LOCATION.test(attr)) {
			if(ant.getCurrentPos().getPositionX().equals(ant.getHasKnownLocation().getPositionX()) && ant.getCurrentPos().getPositionY().equals(ant.getHasKnownLocation().getPositionY())  ) {
				ant.setHasKnownLocation(null);
				this.moveAnt(AntColonyConstants.SEARCH_FOOD, ant, colony, food, nearbyAnt);
				return;
			}
			if(ant.getHasKnownLocation().getPositionX() > ant.getCurrentPos().getPositionX()) {
				ant.getCurrentPos().setPositionX(ant.getCurrentPos().getPositionX() + 1);
			}
			else if(ant.getHasKnownLocation().getPositionX() < ant.getCurrentPos().getPositionX()) {
				ant.getCurrentPos().setPositionX(ant.getCurrentPos().getPositionX() - 1);
			}
			if(ant.getHasKnownLocation().getPositionY() > ant.getCurrentPos().getPositionY()) {
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() + 1);
			}
			else if(ant.getHasKnownLocation().getPositionY() < ant.getCurrentPos().getPositionY()) {
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() - 1);
			}
		}
		else if(AntColonyPredicates.ANT_FOUND.test(attr)) {
			ant.getCurrentPos().setPositionX(nearbyAnt.getCurrentPos().getPositionX());
			ant.getCurrentPos().setPositionY(nearbyAnt.getCurrentPos().getPositionY());
			if(nearbyAnt.getHasKnownLocation() != null) {
				ant.setHasKnownLocation(nearbyAnt.getHasKnownLocation());
			}
		}
		else if(AntColonyPredicates.FOOD_FOUND.test(attr)) {
			ant.getCurrentPos().setPositionX(food.getPosition().getPositionX());
			ant.getCurrentPos().setPositionY(food.getPosition().getPositionY());
			food.setFoodQty(food.getFoodQty() - 1);
			ant.setCarryingFood(true);
		}
		else if(AntColonyPredicates.SEARCH_FOOD.test(attr)) {
			
			if(AntColonyPredicates.X_INCREASE.test(ant)) {
				if(colony.getSize().getPositionX().equals(ant.getCurrentPos().getPositionX())) {
					ant.setMovementX(AntColonyConstants.DECREASE);
					checkIfYAxisIncrease(ant, colony);
				}
				else {
					ant.getCurrentPos().setPositionX(ant.getCurrentPos().getPositionX() + 1);
				}
			}
			
			if(AntColonyPredicates.X_DECREASE.test(ant)) {
				if(Integer.valueOf("1").equals(ant.getCurrentPos().getPositionX())) {
					ant.setMovementX(AntColonyConstants.INCREASE);
					checkIfYAxisIncrease(ant, colony);
				}
				else {
					ant.getCurrentPos().setPositionX(ant.getCurrentPos().getPositionX() - 1);
				}
			}
			
		}
	}
	
	/**
	 * checks if An ant has reached the edge of Y axis of the colony
	 * @param ant
	 * @param colony
	 */
	private void checkIfYAxisIncrease(final Ant ant, final Colony colony) {
		if(AntColonyPredicates.Y_INCREASE.test(ant)) {
			if(colony.getSize().getPositionY().equals(ant.getCurrentPos().getPositionY())) {
				ant.setMovementY(AntColonyConstants.DECREASE);
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() - 1);
			}
			else {
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() + 1);
			}
		}
		else if(AntColonyPredicates.Y_DECREASE.test(ant)) {
			if(Integer.valueOf("1").equals(ant.getCurrentPos().getPositionY())) {
				ant.setMovementY(AntColonyConstants.INCREASE);
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() + 1);
			}
			else {
				ant.getCurrentPos().setPositionY(ant.getCurrentPos().getPositionY() - 1);
			}
		}
	}
	
	/**
	 * this function checks if any other ant is visible at this point of time
	 * @param ant current ant
	 * @param colony complete colony
	 * @return returns the nearby ant if present
	 */
	public Ant isAntVisible(final Ant ant, final Colony colony) {
		return colony.getTribes()
		.stream()
		.filter(Objects::nonNull)
		.flatMap(tribe -> tribe.getAnts().stream())
		.filter(Objects::nonNull)
		.filter(ant2 -> !(ant2.getCurrentPos().getPositionX().equals(ant.getCurrentPos().getPositionX()) && ant2.getCurrentPos().getPositionY().equals(ant.getCurrentPos().getPositionY())) )
		.filter(ant2 -> ant2.getCurrentPos().getPositionX().equals(ant.getCurrentPos().getPositionX() - 1) || ant2.getCurrentPos().getPositionX().equals(ant.getCurrentPos().getPositionX() + 1) || ant2.getCurrentPos().getPositionX().equals(ant.getCurrentPos().getPositionX()))
		.filter(ant2 -> ant2.getCurrentPos().getPositionY().equals(ant.getCurrentPos().getPositionY() - 1) || ant2.getCurrentPos().getPositionY().equals(ant.getCurrentPos().getPositionY() + 1) || ant2.getCurrentPos().getPositionY().equals(ant.getCurrentPos().getPositionY()))
		.findFirst()
		.orElse(null);
	}
	
	/**
	 * this function checks if any food is visible at this point of time
	 * @param ant current ant
	 * @param colony complete colony
	 * @return returns the nearby food is present
	 */
	public Food isFoodVisible(final Ant ant, final Colony colony) {
		return colony
				.getFoods()
				.stream()
				.filter(Objects::nonNull)
				.filter(food -> food.getPosition().getPositionX().equals(ant.getCurrentPos().getPositionX() - 1) || food.getPosition().getPositionX().equals(ant.getCurrentPos().getPositionX() + 1) || food.getPosition().getPositionX().equals(ant.getCurrentPos().getPositionX()))
				.filter(food -> food.getPosition().getPositionY().equals(ant.getCurrentPos().getPositionY() - 1) || food.getPosition().getPositionY().equals(ant.getCurrentPos().getPositionY() + 1) || food.getPosition().getPositionY().equals(ant.getCurrentPos().getPositionY()))
				.findFirst()
				.orElse(null);
	}
	
	/**
	 * this function loads an Ant from the given set of values
	 * @param currentPosInputX current position in X axis of an Ant
	 * @param currentPosInputY current position in Y axis of an Ant
	 * @param homePosInputX Home position in X axis of an Ant
	 * @param homePosInputY Home position in Y axis of an Ant
	 * @return
	 */
	public Ant addAnt(final Integer currentPosInputX, final Integer currentPosInputY, final Integer homePosInputX, final Integer homePosInputY) {
		return antDao.addAnt(currentPosInputX, currentPosInputY, homePosInputX, homePosInputY);
	}
	
}
