package com.valtech.antcolony.service;

import com.valtech.antcolony.dto.Colony;

public interface ColonyService {
	
	/**
	 * this function loads a Colony from the given set of inputs
	 * @param sizeInputX length in X axis of a Colony
	 * @param sizeInputY length in Y axis of a Colony
	 * @return
	 */
	public Colony addColony(final Integer sizeInputX, final Integer sizeInputY);
	
	/**
	 * this function checks if the food is still available for ants in the colony
	 * @param colony the colony to be checked
	 * @return result in boolean format
	 */
	public boolean isFoodAvailable(final Colony colony);
}
