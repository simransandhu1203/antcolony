package com.valtech.antcolony.service;

import com.valtech.antcolony.dto.Tribe;

public interface TribeService {
	
	/**
	 * this function loads a Tribe from the given set of inputs
	 * @param posInputX starting position in X axis of a Tribe
	 * @param posInputY starting position in Y axis of a Tribe
	 * @param sizeInputX length in X axis of a Tribe
	 * @param sizeInputY length in Y axis of a Tribe
	 * @return
	 */
	public Tribe addTribe(final Integer posInputX, final Integer posInputY, final Integer sizeInputX, final Integer sizeInputY);
}
