package com.valtech.antcolony.service;

import com.valtech.antcolony.dto.Ant;
import com.valtech.antcolony.dto.Colony;
import com.valtech.antcolony.dto.Food;

public interface AntService {
	
	/**
	 * this function initializes the movement of an ant
	 * @param ant ant to move
	 * @param colony whole colony available
	 */
	public void move(final Ant ant, final Colony colony);
	
	/**
	 * this function moves the ant depending on the current situation of ant
	 * @param attr current situation of ant
	 * @param ant ant to move
	 * @param colony colony available
	 * @param food food available nearby
	 * @param nearbyAnt and present nearby
	 */
	public void moveAnt(final String attr, final Ant ant, final Colony colony, final Food food, final Ant nearbyAnt);
	
	/**
	 * this function checks if any other ant is visible at this point of time
	 * @param ant current ant
	 * @param colony complete colony
	 * @return returns the nearby ant if present
	 */
	public Ant isAntVisible(final Ant ant, final Colony colony);
	
	/**
	 * this function checks if any food is visible at this point of time
	 * @param ant current ant
	 * @param colony complete colony
	 * @return returns the nearby food is present
	 */
	public Food isFoodVisible(final Ant ant, final Colony colony);
	
	/**
	 * this function loads an Ant from the given set of values
	 * @param currentPosInputX current position in X axis of an Ant
	 * @param currentPosInputY current position in Y axis of an Ant
	 * @param homePosInputX Home position in X axis of an Ant
	 * @param homePosInputY Home position in Y axis of an Ant
	 * @return
	 */
	public Ant addAnt(final Integer currentPosInputX, final Integer currentPosInputY, final Integer homePosInputX, final Integer homePosInputY);
}
