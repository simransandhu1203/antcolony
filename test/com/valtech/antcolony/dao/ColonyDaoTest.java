package com.valtech.antcolony.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.valtech.antcolony.dao.impl.DefaultColonyDaoImpl;
import com.valtech.antcolony.dto.Colony;

public class ColonyDaoTest {

	ColonyDao colonyDao = new DefaultColonyDaoImpl();
	
	@Test
	public void testAddColony() {
		Colony colony = colonyDao.addColony(4, 5);
		assertEquals(new Integer(4), colony.getSize().getPositionX());
		assertEquals(new Integer(5), colony.getSize().getPositionY());
	}
	
}
