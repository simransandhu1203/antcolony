package com.valtech.antcolony.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.valtech.antcolony.dao.impl.DefaultTribeDaoImpl;
import com.valtech.antcolony.dto.Tribe;

public class TribeDaoTest {

	TribeDao tribeDao = new  DefaultTribeDaoImpl();
	
	@Test
	public void testAddFood() {
		Tribe tribe = tribeDao.addTribe(2, 2, 4, 4);
		assertEquals(new Integer(2), tribe.getStartPos().getPositionX());
		assertEquals(new Integer(2), tribe.getStartPos().getPositionY());
		assertEquals(new Integer(4), tribe.getSize().getPositionX());
		assertEquals(new Integer(4), tribe.getSize().getPositionY());
	}
	
}
