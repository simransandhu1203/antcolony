package com.valtech.antcolony.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.valtech.antcolony.dao.impl.DefaultFoodDaoImpl;
import com.valtech.antcolony.dto.Food;

public class FoodDaoTest {

	FoodDao foodDao = new DefaultFoodDaoImpl();
	
	@Test
	public void testAddFood() {
		Food food = foodDao.addFood(3, 4, 2);
		assertEquals(new Integer(3), food.getPosition().getPositionX());
		assertEquals(new Integer(4), food.getPosition().getPositionY());
		assertEquals(new Integer(2), food.getFoodQty());
	}
	
}
