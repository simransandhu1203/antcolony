package com.valtech.antcolony.dao;

import org.junit.Test;

import com.valtech.antcolony.dao.impl.DefaultAntDaoImpl;
import com.valtech.antcolony.dto.Ant;

import static org.junit.Assert.*;

public class AntDaoTest {
	
	AntDao antDao = new DefaultAntDaoImpl();

	@Test
	public void testAddAnt() {
		Ant ant = antDao.addAnt(1, 1, 2, 2);
		assertEquals(new Integer(1), ant.getCurrentPos().getPositionX());
		assertEquals(new Integer(1), ant.getCurrentPos().getPositionY());
		assertEquals(new Integer(2), ant.getHome().getPosition().getPositionX());
		assertEquals(new Integer(2), ant.getHome().getPosition().getPositionY());
		assertEquals(false, ant.isCarryingFood());
	}
}
