package com.valtech.antcolony.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AntColonyUtilsTest {

	@Test
	public void testAddAnt() {
		assertEquals(true, AntColonyUtils.isStringInteger("2"));
		assertEquals(false, AntColonyUtils.isStringInteger("2b"));
	}
	
}
