package com.valtech.colony.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.valtech.antcolony.dto.Ant;
import com.valtech.antcolony.dto.Colony;
import com.valtech.antcolony.dto.Position;
import com.valtech.antcolony.dto.Tribe;
import com.valtech.antcolony.service.AntService;
import com.valtech.antcolony.service.ColonyService;
import com.valtech.antcolony.service.FoodService;
import com.valtech.antcolony.service.TribeService;
import com.valtech.antcolony.service.impl.DefaultAntServiceImpl;
import com.valtech.antcolony.service.impl.DefaultColonyServiceImpl;
import com.valtech.antcolony.service.impl.DefaultFoodServiceImpl;
import com.valtech.antcolony.service.impl.DefaultTribeServiceImpl;

public class AntServiceTest {

	AntService antService = new DefaultAntServiceImpl();
	ColonyService colonyService = new DefaultColonyServiceImpl();
	FoodService foodService = new DefaultFoodServiceImpl();
	TribeService tribeService = new DefaultTribeServiceImpl();
	
	@Test
	public void testAddAnt() {
		Ant ant = antService.addAnt(1, 1, 2, 2);
		assertEquals(new Integer(1), ant.getCurrentPos().getPositionX());
		assertEquals(new Integer(1), ant.getCurrentPos().getPositionY());
		assertEquals(new Integer(2), ant.getHome().getPosition().getPositionX());
		assertEquals(new Integer(2), ant.getHome().getPosition().getPositionY());
		assertEquals(false, ant.isCarryingFood());
	}
	
	@Test
	public void testIsFoodVisible() {
		Colony colony = colonyService.addColony(5, 5);
		colony.getFoods().add(foodService.addFood(3, 3, 1));
		Ant ant = antService.addAnt(2, 3, 4, 4);
		assertEquals(new Integer(3), antService.isFoodVisible(ant, colony).getPosition().getPositionX());
		assertEquals(new Integer(3), antService.isFoodVisible(ant, colony).getPosition().getPositionY());
	}
	
	@Test
	public void testIsAntVisible() {
		Colony colony = colonyService.addColony(5, 5);
		final Tribe tribe = tribeService.addTribe(1, 1, 3, 3);
		colony.getTribes().add(tribe);
		final Ant ant1 = antService.addAnt(2, 2, 2, 2);
		final Ant ant2 = antService.addAnt(3, 3, 3, 3);
		tribe.getAnts().add(ant1);
		tribe.getAnts().add(ant2);
		assertEquals(new Integer(3), antService.isAntVisible(ant1, colony).getCurrentPos().getPositionX());
	}
	
	@Test
	public void testTowardsHome() {
		Colony colony = colonyService.addColony(5, 5);
		final Tribe tribe = tribeService.addTribe(1, 1, 3, 3);
		colony.getTribes().add(tribe);
		final Ant ant1 = antService.addAnt(2, 2, 3, 3);
		ant1.setCarryingFood(true);
		tribe.getAnts().add(ant1);
		antService.move(ant1, colony);
		assertEquals(new Integer(3), ant1.getCurrentPos().getPositionX());
		assertEquals(new Integer(3), ant1.getCurrentPos().getPositionY());
	}
	
	@Test
	public void testTowardsLocation() {
		Colony colony = colonyService.addColony(5, 5);
		final Tribe tribe = tribeService.addTribe(1, 1, 3, 3);
		colony.getTribes().add(tribe);
		final Ant ant1 = antService.addAnt(2, 2, 3, 3);
		final Position position = new Position();
		position.setPositionX(1);
		position.setPositionY(1);
		ant1.setHasKnownLocation(position);
		tribe.getAnts().add(ant1);
		antService.move(ant1, colony);
		assertEquals(new Integer(1), ant1.getCurrentPos().getPositionX());
		assertEquals(new Integer(1), ant1.getCurrentPos().getPositionY());
	}
	
	@Test
	public void testFoodFound() {
		Colony colony = colonyService.addColony(5, 5);
		final Tribe tribe = tribeService.addTribe(1, 1, 3, 3);
		colony.getTribes().add(tribe);
		final Ant ant1 = antService.addAnt(1, 1, 3, 3);
		tribe.getAnts().add(ant1);
		colony.getFoods().add(foodService.addFood(2, 2, 1));
		antService.move(ant1, colony);
		assertEquals(new Integer(2), ant1.getCurrentPos().getPositionX());
		assertEquals(new Integer(2), ant1.getCurrentPos().getPositionY());
		assertEquals(new Integer(0), colony.getFoods().get(0).getFoodQty());
	}
	
	@Test
	public void testAntFound() {
		Colony colony = colonyService.addColony(5, 5);
		final Tribe tribe = tribeService.addTribe(1, 1, 3, 3);
		colony.getTribes().add(tribe);
		final Ant ant1 = antService.addAnt(2, 2, 2, 2);
		final Ant ant2 = antService.addAnt(3, 3, 3, 3);
		tribe.getAnts().add(ant1);
		tribe.getAnts().add(ant2);
		antService.move(ant1, colony);
		assertEquals(new Integer(3), ant1.getCurrentPos().getPositionX());
		assertEquals(new Integer(3), ant1.getCurrentPos().getPositionY());
	}
	
	@Test
	public void testSearchFood() {
		Colony colony = colonyService.addColony(5, 5);
		colony.getFoods().add(foodService.addFood(4, 4, 1));
		final Tribe tribe = tribeService.addTribe(1, 1, 3, 3);
		colony.getTribes().add(tribe);
		final Ant ant1 = antService.addAnt(2, 2, 2, 2);
		tribe.getAnts().add(ant1);
		antService.move(ant1, colony);
		assertEquals(new Integer(3), ant1.getCurrentPos().getPositionX());
		assertEquals(new Integer(2), ant1.getCurrentPos().getPositionY());
	}
	
}
