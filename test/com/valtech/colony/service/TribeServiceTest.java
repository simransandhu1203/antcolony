package com.valtech.colony.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.valtech.antcolony.dto.Tribe;
import com.valtech.antcolony.service.TribeService;
import com.valtech.antcolony.service.impl.DefaultTribeServiceImpl;

public class TribeServiceTest {

	TribeService tribeService = new DefaultTribeServiceImpl();
	
	@Test
	public void testAddFood() {
		Tribe tribe = tribeService.addTribe(2, 2, 4, 4);
		assertEquals(new Integer(2), tribe.getStartPos().getPositionX());
		assertEquals(new Integer(2), tribe.getStartPos().getPositionY());
		assertEquals(new Integer(4), tribe.getSize().getPositionX());
		assertEquals(new Integer(4), tribe.getSize().getPositionY());
	}
	
}
