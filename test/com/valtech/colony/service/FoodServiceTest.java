package com.valtech.colony.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.valtech.antcolony.dto.Food;
import com.valtech.antcolony.service.FoodService;
import com.valtech.antcolony.service.impl.DefaultFoodServiceImpl;

public class FoodServiceTest {
	
	FoodService foodService = new DefaultFoodServiceImpl();
	
	@Test
	public void testAddFood() {
		Food food = foodService.addFood(3, 4, 2);
		assertEquals(new Integer(3), food.getPosition().getPositionX());
		assertEquals(new Integer(4), food.getPosition().getPositionY());
		assertEquals(new Integer(2), food.getFoodQty());
	}

}
