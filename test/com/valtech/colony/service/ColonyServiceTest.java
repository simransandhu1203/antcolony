package com.valtech.colony.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.valtech.antcolony.dto.Colony;
import com.valtech.antcolony.service.ColonyService;
import com.valtech.antcolony.service.FoodService;
import com.valtech.antcolony.service.impl.DefaultColonyServiceImpl;
import com.valtech.antcolony.service.impl.DefaultFoodServiceImpl;

public class ColonyServiceTest {
	
	ColonyService colonyService = new DefaultColonyServiceImpl();
	FoodService foodService = new DefaultFoodServiceImpl();
	
	@Test
	public void testAddColony() {
		Colony colony = colonyService.addColony(4, 5);
		assertEquals(new Integer(4), colony.getSize().getPositionX());
		assertEquals(new Integer(5), colony.getSize().getPositionY());
	}
	
	@Test
	public void testIsFoodAvailable() {
		Colony colony = colonyService.addColony(5, 5);
		colony.getFoods().add(foodService.addFood(3, 3, 1));
		assertEquals(true, colonyService.isFoodAvailable(colony));
	}

}
